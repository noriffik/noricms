using System.Collections.Generic;
using System.Reflection;

namespace NoriCms.Core.Di.ContainerSpecific
{
    public interface IDiscoveredTypesProvider
    {
        IEnumerable<Assembly> GetDiscoveredAssemblies();

        IEnumerable<TypeInfo> GetDiscoveredTypes();
    }
}