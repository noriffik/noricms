namespace NoriCms.Core.Di.ContainerSpecific
{
    public interface IContainerBuilder
    {
        void Build();
    }
}