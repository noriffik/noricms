using NoriCms.Core.Di.ContainerSpecific;

namespace NoriCms.Core.Di.AppDi
{
    public interface IDependencyRegistration
    {
        void Register(IContainerRegister container);
    }
}