namespace NoriCms.Core.Di.AppDi
{
    public interface IInjectionFactory<out T>
    {
        T Create();
    }
}