namespace NoriCms.Core.Di.AppDi
{
    public class InstanceLifetime
    {
        public static readonly InstanceLifetime Singleton = new InstanceLifetime();

        public static readonly InstanceLifetime Transient = new InstanceLifetime();

        public static readonly InstanceLifetime Scoped = new InstanceLifetime();
    }
}