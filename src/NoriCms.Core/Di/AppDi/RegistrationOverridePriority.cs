namespace NoriCms.Core.Di.AppDi
{
    public enum RegistrationOverridePriority
    {
        Low = -50,
        Normal = 0,
        High = 50
    }
}