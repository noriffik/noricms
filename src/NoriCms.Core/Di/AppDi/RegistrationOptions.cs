namespace NoriCms.Core.Di.AppDi
{
    public class RegistrationOptions
    {
        public RegistrationOptions()
        {
            Lifetime = InstanceLifetime.Transient;
        }

        public bool ReplaceExisting { get; set; }

        public InstanceLifetime Lifetime { get; set; }

        public int RegistrationOverridePriority { get; set; }

        public static RegistrationOptions Override(RegistrationOverridePriority? overridePriority = null)
        {
            var options = new RegistrationOptions {ReplaceExisting = true};

            if (overridePriority.HasValue)
            {
                options.RegistrationOverridePriority = (int)overridePriority;
            }

            return options;
        }

        public static RegistrationOptions SingletonScope()
        {
            return new RegistrationOptions() { Lifetime = InstanceLifetime.Singleton };
        }

        public static RegistrationOptions TransientScope()
        {
            return new RegistrationOptions() { Lifetime = InstanceLifetime.Transient };
        }

        public static RegistrationOptions Scoped()
        {
            return new RegistrationOptions() { Lifetime = InstanceLifetime.Scoped };
        }
    }
}