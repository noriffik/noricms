using System;
using System.Threading.Tasks;

namespace NoriCms.Core.Caching
{
    public interface IObjectCache
    {
        T Get<T>(string key);
        T GetOrAdd<T>(string key, Func<T> getter, DateTimeOffset? expiry = null);
        Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> getter, DateTimeOffset? expiry = null);
        void Clear(string key = null);
    }
}