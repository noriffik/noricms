using NoriCms.Core.Caching.InMemory;
using NoriCms.Core.Di.AppDi;
using NoriCms.Core.Di.ContainerSpecific;

namespace NoriCms.Core.Caching.Bootstrap
{
    public class CachingDependencyRegistration: IDependencyRegistration
    {
        public void Register(IContainerRegister container)
        {
            var cacheMode = container.Configuration.GetValue("NoriCms:InMemoryObjectCache:CacheMode", InMemoryObjectCacheMode.Persitent);

            if (cacheMode == InMemoryObjectCacheMode.PerScope)
            {
                container.RegisterScoped<IObjectCacheFactory, InMemoryObjectCacheFactory>();
            }
            else
            {
                container.RegisterSingleton<IObjectCacheFactory, InMemoryObjectCacheFactory>();
            }
        }
    }
}