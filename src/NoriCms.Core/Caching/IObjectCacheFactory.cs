namespace NoriCms.Core.Caching
{
    public interface IObjectCacheFactory
    {
        IObjectCache Get(string cacheNamespace);

        void Clear();
    }
}