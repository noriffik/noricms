using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using NoriCms.Core.Configuration;

namespace NoriCms.Core.Caching.InMemory
{
    public class InMemoryObjectCacheFactory : IObjectCacheFactory
    {
        private readonly ClearableMemoryCache _memoryCache;
        private readonly InMemoryObjectCacheSettings _inMemoryObjectCacheSettings;

        public InMemoryObjectCacheFactory(IOptions<MemoryCacheOptions> optionsAccessor,
            InMemoryObjectCacheSettings inMemoryObjectCacheSettings)
        {
            _memoryCache = new ClearableMemoryCache(optionsAccessor);
            _inMemoryObjectCacheSettings = inMemoryObjectCacheSettings;
        }

        public IObjectCache Get(string cacheNamespace)
        {
            if (_inMemoryObjectCacheSettings.CacheMode == InMemoryObjectCacheMode.Off)
            {
                return new CacheDisabledObjectCache();
            }

            return new InMemoryObjectCache(_memoryCache, cacheNamespace);
        }

        public void Clear()
        {
            _memoryCache.ClearAll();
        }
    }
    
    public class InMemoryObjectCacheSettings : NoriCmsConfigurationSettingsBase
    {
        public InMemoryObjectCacheMode CacheMode { get; set; } = InMemoryObjectCacheMode.Persitent;
    }
}