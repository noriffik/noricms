using NoriCms.Core.Data.Internal;
using NoriCms.Core.Data.SimpleDatabase;
using NoriCms.Core.Di.AppDi;
using NoriCms.Core.Di.ContainerSpecific;

namespace NoriCms.Core.Data.Bootstrap
{
    public class DataTimeDependencyRegistration : IDependencyRegistration
    {
        public void Register(IContainerRegister container)
        {
            container
                .RegisterScoped<ICmsDbConnectionManager, CmsDbConnectionManager>()
                .RegisterScoped<ITransactionScopeManager, DefaultTransactionScopeManager>()
                .RegisterScoped<ICmsDatabase, CmsSqlDatabase>()
                .RegisterScoped<ITransactionScopeFactory, TransactionScopeFactory>();
        }
    }
}