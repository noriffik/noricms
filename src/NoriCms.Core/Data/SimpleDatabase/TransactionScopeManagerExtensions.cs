using NoriCms.Core.Data.Internal;

namespace NoriCms.Core.Data.SimpleDatabase
{
    public static class TransactionScopeManagerExtensions
    {
        public static ITransactionScope Create(this ITransactionScopeManager transactionScopeManager, IDatabase database)
        {
            return transactionScopeManager.Create(database.GetDbConnection());
        }
    }
}