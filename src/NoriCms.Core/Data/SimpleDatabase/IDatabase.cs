using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace NoriCms.Core.Data.SimpleDatabase
{
    public interface IDatabase
    {
        DbConnection GetDbConnection();

        Task ExecuteAsync(string sql, params SqlParameter[] sqlParams);

        Task<ICollection<TEntity>> ReadAsync<TEntity>(string sql, Func<SqlDataReader, TEntity> map, params SqlParameter[] sqlParams);
    }
}