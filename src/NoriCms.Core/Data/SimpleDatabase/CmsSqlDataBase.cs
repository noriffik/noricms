using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace NoriCms.Core.Data.SimpleDatabase
{
    public class CmsSqlDatabase : ICmsDatabase, IDisposable
    {
        private readonly SqlDatabase _sqlDatabase;

        public CmsSqlDatabase(ICmsDbConnectionManager cmsDbConnectionFactory)
        {
            var dbConnection = cmsDbConnectionFactory.GetShared();

            if (dbConnection is SqlConnection connection)
            {
                _sqlDatabase = new SqlDatabase(connection);
            }
            else
            {
                throw new NotSupportedException($"{nameof(SqlDatabase)} only supports SqlConnection. {nameof(ICmsDbConnectionManager)} returned connection of type {dbConnection.GetType().FullName}");
            }
        }
        
        public DbConnection GetDbConnection()
        {
            return _sqlDatabase.GetDbConnection();
        }
        
        public Task ExecuteAsync(string sql, params SqlParameter[] sqlParams)
        {
            return _sqlDatabase.ExecuteAsync(sql, sqlParams);
        }
        
        public Task<ICollection<TEntity>> ReadAsync<TEntity>(string sql, 
            Func<SqlDataReader, TEntity> mapper, 
            params SqlParameter[] sqlParams)
        {

            return _sqlDatabase.ReadAsync(sql, mapper, sqlParams);
        }

        public void Dispose()
        {
            _sqlDatabase?.Dispose();
        }
    }
}