using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;

namespace NoriCms.Core.Data.SimpleDatabase
{
    public class SqlDatabase : IDisposable, IDatabase
    {
        private readonly SqlConnection _sqlConnection;

        public SqlDatabase(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public DbConnection GetDbConnection()
        {
            return _sqlConnection;
        }

        public async Task ExecuteAsync(string sql, params SqlParameter[] sqlParams)
        {
            var isInitialStateClosed = IsClosed();
            if (isInitialStateClosed)
            {
                _sqlConnection.Open();
            }

            await using (var sqlCmd = new SqlCommand(sql, _sqlConnection))
            {
                if (sqlParams != null && sqlParams.Any())
                {
                    sqlCmd.Parameters.AddRange(sqlParams);
                }
                await sqlCmd.ExecuteNonQueryAsync();
            }

            if (isInitialStateClosed)
            {
                _sqlConnection.Close();
            }
        }

        public async Task<ICollection<TEntity>> ReadAsync<TEntity>(string sql, 
            Func<SqlDataReader, TEntity> mapper, 
            params SqlParameter[] sqlParams)
        {
            var isInitialStateClosed = IsClosed();
            if (isInitialStateClosed)
            {
                _sqlConnection.Open();
            }

            var result = new List<TEntity>();

            await using (var sqlCmd = new SqlCommand(sql, _sqlConnection))
            {
                if (sqlParams != null && sqlParams.Any())
                {
                    sqlCmd.Parameters.AddRange(sqlParams);
                }

                await using (var reader = await sqlCmd.ExecuteReaderAsync())
                {
                    while (reader.Read())
                    {
                        var mapped = mapper(reader);
                        result.Add(mapped);
                    }
                }
            }

            if (isInitialStateClosed)
            {
                _sqlConnection.Close();
            }

            return result;
        }

        private bool IsClosed()
        {
            return _sqlConnection.State == ConnectionState.Closed;
        }

        public void Dispose()
        {
            if (_sqlConnection != null && _sqlConnection.State != ConnectionState.Closed)
            {
                _sqlConnection.Close();
            }
        }
    }
}