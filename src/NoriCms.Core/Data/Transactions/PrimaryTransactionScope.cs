using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public class PrimaryTransactionScope : ITransactionScope, IDisposable
    {
        private readonly DefaultTransactionScopeManager _transactionScopeManager;
        private Queue<Func<Task>> _runOnCompleteActions = new Queue<Func<Task>>();
        private System.Transactions.TransactionScope _innerScope;

        public PrimaryTransactionScope(DefaultTransactionScopeManager transactionScopeManager,
            Func<System.Transactions.TransactionScope> transactionScopeFactory)
        {
            _transactionScopeManager = transactionScopeManager;

            _innerScope = transactionScopeFactory();
        }
        
        public async Task CompleteAsync()
        {
            _innerScope.Complete();

            // Dispose of the inner scope so transactions are freed up for
            // code running in the on-complete actions
            DisposeInnerScope();

            // Run all actions
            while (_runOnCompleteActions.Count > 0)
            {
                var item = _runOnCompleteActions.Dequeue();
                await item();
            }
        }
        
        public void Dispose()
        {
            DisposeInnerScope();
        }

        private void DisposeInnerScope()
        {
            if (_innerScope == null) return;
            var scopeToDispose = _innerScope;
            _innerScope = null;

            // De-register this scope as the primary transaction so others can be created
            _transactionScopeManager?.DeregisterTransaction(this);

            // Dispose of the EF transaction which should tidy itself up.
            scopeToDispose.Dispose();
        }

        public void QueueCompletionTask(Func<Task> actionToQueue)
        {
            _runOnCompleteActions.Enqueue(actionToQueue);
        }

        public void QueueCompletionTask(Action actionToQueue)
        {
            _runOnCompleteActions.Enqueue(() =>
            {
                actionToQueue();
                return Task.CompletedTask;
            });
        }
    }
}