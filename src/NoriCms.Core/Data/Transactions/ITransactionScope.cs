using System;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public interface ITransactionScope : IDisposable
    {
        Task CompleteAsync();
        void QueueCompletionTask(Func<Task> actionToQueue);
        void QueueCompletionTask(Action actionToQueue);
    }
}