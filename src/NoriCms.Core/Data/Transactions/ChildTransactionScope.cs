using System;
using System.Threading.Tasks;
using System.Transactions;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public class ChildTransactionScope : ITransactionScope, IDisposable
    {
        private readonly PrimaryTransactionScope _primaryTransactionScope;
        private readonly TransactionScope _innerScope;

        public ChildTransactionScope(PrimaryTransactionScope primaryTransactionScope,
            Func<System.Transactions.TransactionScope> transactionScopeFactory)
        {
            _primaryTransactionScope = primaryTransactionScope;
            _innerScope = transactionScopeFactory();
        }

        public Task CompleteAsync()
        {
            _innerScope.Complete();
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _innerScope.Dispose();
        }

        public void QueueCompletionTask(Func<Task> actionToQueue)
        {
            _primaryTransactionScope.QueueCompletionTask(actionToQueue);
        }

        
        public void QueueCompletionTask(Action actionToQueue)
        {
            _primaryTransactionScope.QueueCompletionTask(actionToQueue);
        }
    }
}