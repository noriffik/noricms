using System;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public interface ITransactionScopeManager : IDisposable
    {
        ITransactionScope Create(DbConnection dbConnection);
        ITransactionScope Create(DbContext dbContext);
        void QueueCompletionTask(DbConnection dbConnection, Action actionToQueue);
        Task QueueCompletionTaskAsync(DbConnection dbConnection, Func<Task> actionToQueue);
        void QueueCompletionTask(DbContext dbContext, Action actionToQueue);
        Task QueueCompletionTaskAsync(DbContext dbContext, Func<Task> actionToQueue);
    }
}