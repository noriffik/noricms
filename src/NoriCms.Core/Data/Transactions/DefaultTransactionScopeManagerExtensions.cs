using System;
using System.Data.Common;
using System.Transactions;
using Microsoft.EntityFrameworkCore;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public static class DefaultTransactionScopeManagerExtensions
    {
        public static ITransactionScope Create(this ITransactionScopeManager transactionScopeManager,
            DbConnection dbConnection,
            TransactionScopeOption transactionScopeOption = TransactionScopeOption.Required,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var defaultTransactionScopeManager = CastDefaultTransactionScopeManager(transactionScopeManager);

            return defaultTransactionScopeManager.Create(dbConnection, transactionScopeOption, isolationLevel);
        }

        public static ITransactionScope Create(this ITransactionScopeManager transactionScopeManager,
            DbConnection dbConnection, Func<TransactionScope> transactionScopeFactory)
        {
            var defaultTransactionScopeManager = CastDefaultTransactionScopeManager(transactionScopeManager);

            return defaultTransactionScopeManager.Create(dbConnection, transactionScopeFactory);
        }

        public static ITransactionScope Create(this ITransactionScopeManager transactionScopeManager, 
            DbContext dbContext,
            TransactionScopeOption transactionScopeOption = TransactionScopeOption.Required,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var defaultTransactionScopeManager = CastDefaultTransactionScopeManager(transactionScopeManager);

            return defaultTransactionScopeManager.Create(dbContext, transactionScopeOption, isolationLevel);
        }

        public static ITransactionScope Create(this ITransactionScopeManager transactionScopeManager, 
            DbContext dbContext, Func<TransactionScope> transactionScopeFactory
        )
        {
            var defaultTransactionScopeManager = CastDefaultTransactionScopeManager(transactionScopeManager);

            return defaultTransactionScopeManager.Create(dbContext, transactionScopeFactory);
        }

        private static IDefaultTransactionScopeManager CastDefaultTransactionScopeManager(
            ITransactionScopeManager transactionScopeManager)
        {
            if (!(transactionScopeManager is IDefaultTransactionScopeManager defaultTransactionScopeManager))
            {
                throw new ArgumentException(
                    $"{nameof(transactionScopeManager)} must of type {nameof(IDefaultTransactionScopeManager)} to be used with a method that includes transactionscope options.");
            }

            return defaultTransactionScopeManager;
        }
    }
}