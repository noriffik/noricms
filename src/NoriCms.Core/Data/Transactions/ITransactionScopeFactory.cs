using System.Transactions;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public interface ITransactionScopeFactory
    {
        TransactionScope Create(); 
        TransactionScope Create(TransactionScopeOption transactionScopeOption,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
    }
}