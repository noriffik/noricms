using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public interface IDefaultTransactionScopeManager : ITransactionScopeManager
    {
        ITransactionScope Create(DbConnection dbConnection,
            System.Transactions.TransactionScopeOption transactionScopeOption =
                System.Transactions.TransactionScopeOption.Required,
            System.Transactions.IsolationLevel isolationLevel = System.Transactions.IsolationLevel.ReadCommitted);

        ITransactionScope Create(DbConnection dbConnection,
            Func<System.Transactions.TransactionScope> transactionScopeFactory);

        ITransactionScope Create(DbContext dbContext,
            System.Transactions.TransactionScopeOption transactionScopeOption =
                System.Transactions.TransactionScopeOption.Required,
            System.Transactions.IsolationLevel isolationLevel = System.Transactions.IsolationLevel.ReadCommitted);

        ITransactionScope Create(DbContext dbContext,
            Func<System.Transactions.TransactionScope> transactionScopeFactory);
    }
}