using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.EntityFrameworkCore;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data.Internal
{
    public class DefaultTransactionScopeManager : IDefaultTransactionScopeManager
    {
        private Dictionary<int, PrimaryTransactionScope> _primaryTransactionScopes = new Dictionary<int, PrimaryTransactionScope>();
        private readonly ITransactionScopeFactory _transactionScopeFactory;

        public DefaultTransactionScopeManager(ITransactionScopeFactory transactionScopeFactory)
        {
            _transactionScopeFactory = transactionScopeFactory;
        }

        public ITransactionScope Create(DbConnection dbConnection)
        {
            return Create(dbConnection, CreateScopeFactory());
        }

        public ITransactionScope Create(DbConnection dbConnection, TransactionScopeOption transactionScopeOption = TransactionScopeOption.Required,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var transactionScopeFactory = CreateScopeFactory(transactionScopeOption, isolationLevel);
            return Create(dbConnection, transactionScopeFactory);
        }

        public ITransactionScope Create(DbConnection dbConnection, Func<System.Transactions.TransactionScope> transactionScopeFactory)
        {
            if (dbConnection == null) throw new ArgumentNullException(nameof(dbConnection));
            if (transactionScopeFactory == null) throw new ArgumentNullException(nameof(transactionScopeFactory));

            ITransactionScope scope;
            var connectionHash = dbConnection.GetHashCode();
            var primaryScope = _primaryTransactionScopes.GetOrDefault(connectionHash);

            if (primaryScope == null)
            {
                primaryScope = new PrimaryTransactionScope(this, transactionScopeFactory);
                _primaryTransactionScopes.Add(connectionHash, primaryScope);
                scope = primaryScope;
            }
            else
            {
                scope = new ChildTransactionScope(primaryScope, transactionScopeFactory);
            }

            return scope;
        }

        public ITransactionScope Create(DbContext dbContext)
        {
            var connection = dbContext.Database.GetDbConnection();

            return Create(connection);
        }

        public ITransactionScope Create(DbContext dbContext,
            TransactionScopeOption transactionScopeOption = TransactionScopeOption.Required,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var connection = dbContext.Database.GetDbConnection();

            return Create(connection, transactionScopeOption, isolationLevel);
        }

        public ITransactionScope Create(DbContext dbContext, Func<TransactionScope> transactionScopeFactory)
        {
            var connection = dbContext.Database.GetDbConnection();

            return Create(connection, transactionScopeFactory);
        }

        public void QueueCompletionTask(DbConnection dbConnection, Action actionToQueue)
        {
            var connectionHash = dbConnection.GetHashCode();
            var scope = _primaryTransactionScopes.GetOrDefault(connectionHash);

            // No scope, execute immediately
            if (scope == null)
            {
                actionToQueue();
            }
            else
            {
                scope.QueueCompletionTask(actionToQueue);
            }
        }

        public Task QueueCompletionTaskAsync(DbConnection dbConnection, Func<Task> actionToQueue)
        {
            var connectionHash = dbConnection.GetHashCode();
            var scope = _primaryTransactionScopes.GetOrDefault(connectionHash);

            // No scope, execute immediately
            if (scope == null) return actionToQueue();

            scope.QueueCompletionTask(actionToQueue);

            return Task.CompletedTask;
        }
        
        public void QueueCompletionTask(DbContext dbContext, Action actionToQueue)
        {
            var dbConnection = dbContext.Database.GetDbConnection();
            QueueCompletionTask(dbConnection, actionToQueue);
        }

        public Task QueueCompletionTaskAsync(DbContext dbContext, Func<Task> actionToQueue)
        {
            var dbConnection = dbContext.Database.GetDbConnection();
            return QueueCompletionTaskAsync(dbConnection, actionToQueue);
        }

        internal void DeregisterTransaction(PrimaryTransactionScope scope)
        {
            if (scope == null) throw new ArgumentNullException(nameof(scope));

            var scopeToRemoveKey = _primaryTransactionScopes
                .Where(s => s.Value == scope)
                .Select(s => (int?)s.Key)
                .SingleOrDefault();

            if (scopeToRemoveKey.HasValue)
            {
                _primaryTransactionScopes.Remove(scopeToRemoveKey.Value);
            }
        }

        private Func<TransactionScope> CreateScopeFactory(TransactionScopeOption transactionScopeOption = TransactionScopeOption.Required,
            IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            return () => _transactionScopeFactory.Create(
                transactionScopeOption,
                isolationLevel
            );
        }

        public void Dispose()
        {
            foreach (var scope in _primaryTransactionScopes)
            {
                scope.Value.Dispose();
            }

            _primaryTransactionScopes.Clear();
        }
    }
}