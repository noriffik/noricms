using System;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace NoriCms.Core.Data
{
    public class CmsDbConnectionManager : ICmsDbConnectionManager, IDisposable
    {
        private readonly DatabaseSettings _databaseSettings;
        private DbConnection _dbConnection;
        
        public CmsDbConnectionManager(DatabaseSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }
        
        public DbConnection Create()
        {
            return new SqlConnection(_databaseSettings.ConnectionString);
        }
        
        public DbConnection GetShared()
        {
            if (_dbConnection != null)
            {
                return _dbConnection;
            }

            _dbConnection = Create();

            return _dbConnection;
        }

        public void Dispose()
        {
            if (_dbConnection != null)
            {
                _dbConnection.Dispose();
            }
        }
    }
}