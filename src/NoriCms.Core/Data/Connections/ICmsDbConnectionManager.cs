using System.Data.Common;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Data
{
    public interface ICmsDbConnectionManager
    {
        DbConnection Create();
        DbConnection GetShared();
    }
}