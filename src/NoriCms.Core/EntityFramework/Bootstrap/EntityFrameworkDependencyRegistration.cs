using NoriCms.Core.Di.AppDi;
using NoriCms.Core.Di.ContainerSpecific;
using NoriCms.Core.EntityFramework.Internal;

namespace NoriCms.Core.EntityFramework.Bootstrap
{
    public class EntityFrameworkDependencyRegistration : IDependencyRegistration
    {
        public void Register(IContainerRegister container)
        {
            container
                .Register<IEntityFrameworkSqlExecutor, EntityFrameworkSqlExecutor>()
                .Register<ISqlParameterFactory, SqlParameterFactory>()
                .Register<ICmsDbContextInitializer, CmsDbContextInitializer>();
        }
    }
}