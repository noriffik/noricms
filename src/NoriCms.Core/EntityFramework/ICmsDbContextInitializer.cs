using Microsoft.EntityFrameworkCore;

namespace NoriCms.Core.EntityFramework
{
    public interface ICmsDbContextInitializer
    {
        DbContextOptionsBuilder Configure(DbContext dbContext, DbContextOptionsBuilder optionsBuilder);
    }
}