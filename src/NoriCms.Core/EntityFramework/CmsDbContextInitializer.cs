using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NoriCms.Core.Data;

namespace NoriCms.Core.EntityFramework
{
    public class CmsDbContextInitializer : ICmsDbContextInitializer
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly ICmsDbConnectionManager _cmsDbConnectionFactory;
        private readonly DatabaseSettings _databaseSettings;
        
        public CmsDbContextInitializer(ILoggerFactory loggerFactory,
            ICmsDbConnectionManager cmsDbConnectionFactory,
            DatabaseSettings databaseSettings)
        {
            _loggerFactory = loggerFactory;
            _cmsDbConnectionFactory = cmsDbConnectionFactory;
            _databaseSettings = databaseSettings;
        }
        
        public DbContextOptionsBuilder Configure(DbContext dbContext, DbContextOptionsBuilder optionsBuilder)
        {
            if (dbContext == null) throw new ArgumentEmptyException(nameof(dbContext));
            if (optionsBuilder == null) throw new ArgumentEmptyException(nameof(optionsBuilder));

            optionsBuilder.UseLoggerFactory(_loggerFactory);
            
            var connection = _cmsDbConnectionFactory.GetShared();
            optionsBuilder.UseSqlServer(connection);

            return optionsBuilder;
        }
    }
}