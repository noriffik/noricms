using System;
using Microsoft.Data.SqlClient;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.EntityFramework
{
    public interface ISqlParameterFactory
    {
        SqlParameter CreateOutputParameterByType(string name, Type t);
    }
}