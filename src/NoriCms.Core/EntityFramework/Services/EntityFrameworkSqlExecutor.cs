using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using NoriCms.Core.Data.Internal;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.EntityFramework.Internal
{
    public class EntityFrameworkSqlExecutor : IEntityFrameworkSqlExecutor
    {
        #region constructor

        private readonly ISqlParameterFactory _sqlParameterFactory;
        private readonly ITransactionScopeManager _transactionScopeFactory;

        public EntityFrameworkSqlExecutor(ISqlParameterFactory sqlParameterFactory,
            ITransactionScopeManager transactionScopeFactory)
        {
            _sqlParameterFactory = sqlParameterFactory;
            _transactionScopeFactory = transactionScopeFactory;
        }

        #endregion

        #region ExecuteQuery

        public virtual async Task<T[]> ExecuteQueryAsync<T>(DbContext dbContext, string spName, params SqlParameter[] sqlParams)
            where T : class
        {
            return await CreateQuery<T>(dbContext, spName, sqlParams).ToArrayAsync();
        }

        private IQueryable<T> CreateQuery<T>(DbContext dbContext, string spName, params SqlParameter[] sqlParams)
            where T : class
        {
            if (!sqlParams.Any()) return dbContext.Set<T>().FromSqlRaw(spName);
            var cmd = FormatSqlCommand(spName, sqlParams);

            // Here we used to use SqlQuery() in EF6 but it isn't supported
            // until raw sql access is added to EF we'll have to be restricted 
            // to entities only.
            // see https://github.com/aspnet/EntityFramework/issues/1862
            // and https://github.com/aspnet/EntityFrameworkCore/issues/10753

            // ReSharper disable once CoVariantArrayConversion
            return dbContext.Set<T>().FromSqlRaw(cmd, sqlParams);
        }

        #endregion

        #region ExecuteScalar

        public virtual async Task<T> ExecuteScalarAsync<T>(DbContext dbContext, string spName, params SqlParameter[] sqlParams)
        {
            // Derived from code in https://github.com/dotnet/efcore/issues/1862
            // Needs to be updated when EF Core finally supports ad-hoc queries again
            var databaseFacade = dbContext.Database;
            var concurrencyDetector = databaseFacade.GetService<IConcurrencyDetector>();
            var sql = FormatSqlCommand(spName, sqlParams);

            using (concurrencyDetector.EnterCriticalSection())
            {
                var rawSqlCommand = databaseFacade
                    .GetService<IRawSqlCommandBuilder>()
                    .Build(sql, sqlParams);

                var commandParameters = new RelationalCommandParameterObject(
                    databaseFacade.GetService<IRelationalConnection>(),
                    rawSqlCommand.ParameterValues,
                    null,
                    dbContext,
                    null);

                var result = await rawSqlCommand
                    .RelationalCommand
                    .ExecuteScalarAsync(commandParameters);

                return ParseScalarResult<T>(result);
            }
        }

        private T ParseScalarResult<T>(object result)
        {
            if (result == DBNull.Value) return default(T);

            // If this is a non-null value nullable type, return the converted base type
            var nullableType = Nullable.GetUnderlyingType(typeof(T));
            if (nullableType != null)
            {
                return (T)Convert.ChangeType(result, nullableType);
            }
            return (T)Convert.ChangeType(result, typeof(T));
        }

        #endregion

        #region ExecuteCommand

        public virtual async Task<object> ExecuteCommandAsync(DbContext dbContext, string spName, params SqlParameter[] sqlParams)
        {
            object result = null;

            if (sqlParams.Any())
            {
                var cmd = FormatSqlCommand(spName, sqlParams);
                FormatSqlParameters(sqlParams);
                int rowsAffected = 0;

                await dbContext.Database.CreateExecutionStrategy().Execute(async () =>
                {
                    using var scope = _transactionScopeFactory.Create(dbContext);
                    // ReSharper disable once CoVariantArrayConversion
                    rowsAffected = await dbContext.Database.ExecuteSqlRawAsync(cmd, sqlParams);
                    await scope.CompleteAsync();
                });

                result = GetOutputParamValue(sqlParams) ?? rowsAffected;
            }
            else
            {
                await dbContext.Database.CreateExecutionStrategy().ExecuteAsync(async () =>
                {
                    using var scope = _transactionScopeFactory.Create(dbContext);
                    result = await dbContext.Database.ExecuteSqlRawAsync(spName);
                    await scope.CompleteAsync();
                });
            }

            return result;
        }

        private static object GetOutputParamValue(SqlParameter[] sqlParams)
        {
            var outputParam = sqlParams.FirstOrDefault(p => p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output);
            return outputParam?.Value;
        }

        #endregion

        #region ExecuteCommandWithOutput

        public virtual async Task<T> ExecuteCommandWithOutputAsync<T>(DbContext dbContext, string spName, string outputParameterName, params SqlParameter[] sqlParams)
        {
            var outputParam = CreateOutputParameter<T>(outputParameterName);
            var modifiedParams = MergeParameters(sqlParams, outputParam);

            await ExecuteCommandAsync(dbContext, spName, modifiedParams);

            return ParseOutputParameter<T>(outputParam);
        }

        private SqlParameter[] MergeParameters(SqlParameter[] sqlParams, SqlParameter paramToMerge)
        {
            return sqlParams
                .Union(new[] { paramToMerge })
                .ToArray();
        }

        private T ParseOutputParameter<T>(SqlParameter outputParam)
        {
            if (outputParam.Value == DBNull.Value) return default;

            // If this is a non-null value nullable type, return the converted base type
            var nullableType = Nullable.GetUnderlyingType(typeof(T));
            if (nullableType != null)
            {
                return (T)Convert.ChangeType(outputParam.Value, nullableType);
            }
            return (T)Convert.ChangeType(outputParam.Value, typeof(T));
        }

        private SqlParameter CreateOutputParameter<T>(string outputParameterName)
        {
            return _sqlParameterFactory.CreateOutputParameterByType(outputParameterName, typeof(T));
        }

        #endregion

        #region private helpers

        private void FormatSqlParameters(SqlParameter[] sqlParams)
        {
            foreach (var xmlParam in sqlParams)
            {
                switch (xmlParam.Value)
                {
                    case null:
                        // Nulls need to be DbNull
                        xmlParam.Value = DBNull.Value;
                        break;
                    case XElement:
                        // We need to re-map xml params because EF doesn't seem to like it
                        xmlParam.SqlDbType = SqlDbType.Xml;
                        xmlParam.Value = xmlParam.Value.ToString();
                        break;
                }
            }
        }

        private string FormatSqlCommand(string spName, SqlParameter[] sqlParams)
        {
            var formattedParams = sqlParams
                .Select(p => $"@{p.ParameterName.Trim('@')} {GetParameterDirection(p)}".TrimEnd())
                .ToArray();
            return $"{spName} {string.Join(", ", formattedParams)}";
        }

        private string GetParameterDirection(SqlParameter p)
        {
            if (p.Direction == ParameterDirection.InputOutput
                || p.Direction == ParameterDirection.Output)
            {
                return "out";
            }

            return string.Empty;
        }

        #endregion
    }
}