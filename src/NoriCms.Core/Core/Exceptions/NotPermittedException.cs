using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class NotPermittedException : Exception
    {
        public NotPermittedException()
        {
        }
        public NotPermittedException(string message)
            : base(message)
        {
        }
    }
}