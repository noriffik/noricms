using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class AuthenticationFailedException : Exception
    {
        const string DEFAULT_MESSAGE = "Authentication failed.";

        public AuthenticationFailedException() : this(DEFAULT_MESSAGE)
        {
        }
        public AuthenticationFailedException(string message)
            : base(message)
        {
        }
    }
}