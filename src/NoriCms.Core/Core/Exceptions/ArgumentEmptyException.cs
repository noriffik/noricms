using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class ArgumentEmptyException: ArgumentException
    {
        public ArgumentEmptyException()
        {
        }

        public ArgumentEmptyException(string propertyName)
            : base($"Value cannot be empty. Property name: {propertyName}", propertyName)
        {
        }
    }
}