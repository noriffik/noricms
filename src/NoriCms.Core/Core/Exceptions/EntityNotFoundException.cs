using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException()
        {
        }
        public EntityNotFoundException(string message)
            : base(message)
        {
        }

        public static void ThrowIfNull<TEntity>(TEntity e, object id) where TEntity : class
        {
            if (e == null)
            {
                throw new EntityNotFoundException<TEntity>(id);
            }
        }
    }

    public class EntityNotFoundException<TEntity> : EntityNotFoundException where TEntity : class
    {
        private const string ERROR_MESSAGE = "Entity of type '{0}' and identifier '{1}' could not be found.";

        public object Id { get; private set; }

        public EntityNotFoundException()
        {
        }

        public EntityNotFoundException(object id)
            : base(FormatMessage(id))
        {
            Id = id;
        }


        private static string FormatMessage(object id)
        {
            return string.Format(ERROR_MESSAGE, typeof(TEntity), id);
        }
    }
}