using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class SqlCharValidator
    {
        public static bool IsValid(string stringToValidate, int length)
        {
            return !string.IsNullOrWhiteSpace(stringToValidate)
                   && stringToValidate.Length == length
                   && !stringToValidate.Any(c => c > 255);
        }
    }
}