using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class ListExtensions
    {
        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }

        public static void AddIfNotEmpty(this IList<string> list, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                list.Add(value);
            }
        }

        public static IEnumerable<string> RemoveEmptyAndDuplicates(this IEnumerable<string> list)
        {
            return list.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct();
        }
    }
}