using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> FilterNotNull<T>(this IEnumerable<T?> source)
            where T : struct
        {
            return source
                .Where(s => s.HasValue)
                .Select(s => s.Value);
        }
    }
}