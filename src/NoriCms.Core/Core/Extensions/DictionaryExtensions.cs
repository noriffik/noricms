using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class DictionaryExtensions
    {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key)
        {
            if (key == null) return default;
            return source.TryGetValue(key, out var value) ? value : default;
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey? key)
            where TKey : struct
        {
            if (!key.HasValue) return default;
            return source.TryGetValue(key.Value, out var value) ? value : default;
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey key, TValue def)
        {
            if (key == null) return def;
            return source.TryGetValue(key, out var value) ? value : def;
        }

        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> source, TKey? key, TValue def)
            where TKey : struct
        {
            if (!key.HasValue) return def;
            return source.TryGetValue(key.Value, out var value) ? value : def;
        }

        public static IEnumerable<TValue> FilterByKeys<TKey, TValue>(this IDictionary<TKey, TValue> source, IEnumerable<TKey> keysToFilter)
        {
            return source.FilterAndOrderByKeys(keysToFilter.Distinct());
        }

        public static IEnumerable<TValue> FilterAndOrderByKeys<TKey, TValue>(this IDictionary<TKey, TValue> source, IEnumerable<TKey> orderedKeys)
        {
            if (orderedKeys == null) yield break;

            foreach (var key in orderedKeys)
            {
                TValue value;
                if (source.TryGetValue(key, out value))
                {
                    yield return value;
                }
            }
        }
    }
}