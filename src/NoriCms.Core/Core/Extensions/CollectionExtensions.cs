using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class CollectionExtensions
    {
        public static int RemoveAll<T>(this ICollection<T> collection, Predicate<T> predicate = null)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));

            int count = 0;

            for (var i = collection.Count - 1; i >= 0; i--)
            {
                var el = collection.ElementAt(i);
                if (predicate != null && !predicate(el)) continue;
                collection.Remove(el);
                count++;
            }

            return count;
        }
    }
}