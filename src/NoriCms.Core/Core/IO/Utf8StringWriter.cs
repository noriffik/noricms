using System;
using System.IO;
using System.Text;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.IO
{
    public class Utf8StringWriter : StringWriter
    {
        public Utf8StringWriter() : base()
        {
        }

        public Utf8StringWriter(IFormatProvider formatProvider)
            : base(formatProvider)
        {
        }

        public Utf8StringWriter(StringBuilder sb)
            : base(sb)
        {
        }


        public Utf8StringWriter(StringBuilder sb, IFormatProvider formatProvider)
            : base(sb, formatProvider)
        {
        }

        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }
}