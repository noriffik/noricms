using System;
using System.Text;
// ReSharper disable once CheckNamespace
namespace NoriCms.Core.IO
{
    public class DebugTextWriter : System.IO.TextWriter
    {
        public override void Write(char[] buffer, int index, int count)
        {
            Write(new String(buffer, index, count));
        }

        public override void Write(string value)
        {
            System.Diagnostics.Debug.Write(value);
        }

        public override void Write(char value)
        {
            System.Diagnostics.Debug.Write(value);
        }

        public override Encoding Encoding => Encoding.GetEncoding(0); // default
    }
}