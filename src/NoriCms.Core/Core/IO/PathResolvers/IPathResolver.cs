// ReSharper disable once CheckNamespace

namespace NoriCms.Core
{
    public interface IPathResolver
    {
        string MapPath(string path);
    }
}