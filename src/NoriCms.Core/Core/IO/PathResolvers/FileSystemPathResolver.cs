using System;
using System.IO;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class FileSystemPathResolver : IPathResolver
    {
        public string MapPath(string path)
        {
            var basePath = AppContext.BaseDirectory;

            if (string.IsNullOrWhiteSpace(path)) return basePath;

            path = path.TrimStart('~', '/');

            return Path.Combine(basePath, path);
        }
    }
}