using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NoriCms.Core.Configuration;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class DebugSettings : NoriCmsConfigurationSettingsBase
    {
        public bool DisableRobotsTxt { get; set; }
        
        public DeveloperExceptionPageMode DeveloperExceptionPageMode { get; set; } = DeveloperExceptionPageMode.DevelopmentOnly;

        public bool UseUncompressedResources { get; set; }

        public bool BypassEmbeddedContent { get; set; }

        public string EmbeddedContentPhysicalPathRootOverride { get; set; }

        public bool CanShowDeveloperExceptionPage(IWebHostEnvironment env)
        {
            return DeveloperExceptionPageMode == DeveloperExceptionPageMode.On
                   || DeveloperExceptionPageMode == DeveloperExceptionPageMode.DevelopmentOnly && env.IsDevelopment();
        }
    }
}