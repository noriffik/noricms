// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class DbConstants
    {
        public static string CmsSchema = "NoriCms";

        public static string CmsPluginSchema = "NoriCmsPlugin";

        public static string DefaultAppSchema = "app";
    }
}