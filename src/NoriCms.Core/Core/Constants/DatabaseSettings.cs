using System.ComponentModel.DataAnnotations;
using NoriCms.Core.Configuration;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class DatabaseSettings : NoriCmsConfigurationSettingsBase
    {
        [Required]
        public string ConnectionString { get; set; }
    }
}