// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class DangerousFileFormats
    {
        public static readonly string[] DangerousFileExtensions = new string[] {
            ".ade",".adp",".app",".asp",".bas",".bat",".cer",".chm",".cmd",".cnt",".com",".cpl",".crt",".csh",".der",".exe",".fxp",".gadget",".hlp",".hpj",".hta",".inf",".ins",".isp",".its", ".js", ".jse", 
            ".ksh", ".lnk", ".mad", ".maf", ".mag", ".mam", ".maq", ".mar", ".mas", ".mat", ".mau", ".mav", ".maw", ".mda", ".mdb", ".mde", ".mdt", ".mdw", ".mdz", ".msc", ".msh",".msh1",".msh2",".mshxml",
            ".msh1xml",".msh2xml",".msi", ".msp", ".mst", ".ops", ".osd",".pcd", ".pif", ".plg",".prf", ".prg", ".pst", ".reg", ".scf", ".scr", ".sct", ".shb", ".shs", ".ps1",".ps1xml",".ps2",".ps2xml",
            ".psc1",".psc2",".tmp", ".url", ".vb", ".vbe", ".vbp",".vbs", ".vsmacros ",".vsw",".ws", ".wsc", ".wsf", ".wsh", ".xnk",
        };
        
        public static readonly string[] DangerousMimeTypes = new string[] {
            "application/x-msdownload",
            "application/x-msdos-program",
            "application/x-msdos-windows",
            "application/x-download",
            "application/bat",
            "application/x-bat",
            "application/com",
            "application/x-com",
            "application/exe",
            "application/x-exe",
            "application/x-winexe",
            "application/x-winhlp",
            "application/x-winhelp",
            "application/x-javascript",
            "application/hta",
            "application/x-ms-shortcut",
            "application/octet-stream",
            "vms/exe",
            "text/javascript","text/scriptlet"
        };
    }
}