// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public interface IOrderedTask
    {
        int Ordering { get; }
    }
}