using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public interface IRunBeforeTask
    {
        ICollection<Type> RunBefore { get; }
    }
}