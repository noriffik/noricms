using System;
using System.Collections.Generic;
// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class TopologicalSorter
    {
        public static ICollection<TItem> Sort<TItem>(ICollection<TItem> source,
            Func<TItem, IEnumerable<TItem>, IEnumerable<TItem>> dependencySelector,
            bool throwOnCyclicDependency)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (dependencySelector == null) throw new ArgumentNullException(nameof(dependencySelector));

            var sorted = new List<TItem>();
            var visited = new HashSet<TItem>();

            foreach (var item in source)
            {
                Visit(item, visited, sorted, source, dependencySelector, throwOnCyclicDependency);
            }

            return sorted;
        }

        private static void Visit<TItem>(TItem item, HashSet<TItem> visited, 
            List<TItem> sorted, ICollection<TItem> source,
            Func<TItem, IEnumerable<TItem>, IEnumerable<TItem>> dependencySelector,
            bool throwOnCyclicDependency)
        {
            if (!visited.Contains(item))
            {
                visited.Add(item);

                foreach (var dep in dependencySelector(item, source))
                {
                    Visit(dep, visited, sorted, source, dependencySelector, throwOnCyclicDependency);
                }

                sorted.Add(item);
            }
            else
            {
                if (!throwOnCyclicDependency || sorted.Contains(item)) return;
                var msg = $"A cyclic dependency has been detected. The sorted collection already contains the item '{item?.ToString()}' ";
                throw new CyclicDependencyException(msg);
            }
        }
    }
}