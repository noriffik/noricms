using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public interface IRunAfterTask
    {
        ICollection<Type> RunAfter { get; }
    }
}