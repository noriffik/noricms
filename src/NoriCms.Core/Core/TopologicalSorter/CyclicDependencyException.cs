using System;
// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class CyclicDependencyException : Exception
    {
        public CyclicDependencyException()
            : base(CreateMessage())
        {
        }

        public CyclicDependencyException(string message)
            : base(CreateMessage(message))
        {
        }

        public CyclicDependencyException(string message, Exception exception)
            : base(CreateMessage(message), exception)
        {
        }

        private static string CreateMessage(string message = null)
        {
            const string DEFAULT_MESSAGE = "Cyclic dependency detected.";

            return string.IsNullOrWhiteSpace(message) ? DEFAULT_MESSAGE : message;
        }
    }
}