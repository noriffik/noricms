// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public enum BasicHtmlFormatOption
    {
        None = 0,
        LinksToNewWindow = 1,
        LinksNoFollow = 2
    }
}