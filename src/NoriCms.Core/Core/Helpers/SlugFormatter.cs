using System.Text.RegularExpressions;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class SlugFormatter
    {
        public static string ToSlug(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return string.Empty;

            string str = TextFormatter.RemoveDiacritics(s)
                .ToLower()
                .Replace("&", " and ");
            str = Regex.Replace(str, @"[/\\\.,\+=–—:_]", " ");
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = Regex.Replace(str, @"\s", "-");
            str = Regex.Replace(str, @"--+", "-");

            return str.Trim('-');
        }

        public static string CamelCaseToSlug(string s)
        {
            if (s == null) return string.Empty;

            // first separate any upper case characters
            var conveted = Regex.Replace(s, "[A-Z](?![A-Z])|[A-Z]+(?![a-z])", m => $"-{m.Value.ToLowerInvariant()}");

            // If there's any other formatting issues, the regular slugify should pick them up.
            return ToSlug(conveted);
        }
    }
}