using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Html;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class HtmlFormatter
    {
        public static string ConvertLineBreaksToBrTags(string stIn)
        {
            return stIn
                .Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None)
                .Aggregate((a, b) => $"{a}<br/>{b}");
        }

        public static string ConvertUrlsToLinks(string text, BasicHtmlFormatOption formatOptions = BasicHtmlFormatOption.None)
        {
            var reg = new Regex(@"[""'=]?(http://|ftp://|https://|www\.|ftp\.[\w]+)([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])",
                RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline | RegexOptions.IgnoreCase);
            
            var matchEval = new MatchEvaluator((match) =>
            {
                string href = match.Value;

                // if string starts within an HREF don't expand it
                if (href.StartsWith("=") || href.StartsWith("'") || href.StartsWith("\"")) return href;

                string displayText = href;

                if (href.IndexOf("://", StringComparison.Ordinal) < 0)
                {
                    if (href.StartsWith("www."))
                    {
                        href = $"http://{href}";
                    }
                    else if (href.StartsWith("ftp"))
                    {
                        href = $"ftp://{href}";
                    }
                    else if (href.IndexOf("@", StringComparison.Ordinal) > -1)
                    {
                        href = $"mailto:{href}";
                    }
                }

                string additionalAttributes = string.Empty;

                if (formatOptions.HasFlag(BasicHtmlFormatOption.LinksNoFollow) && formatOptions.HasFlag(BasicHtmlFormatOption.LinksToNewWindow))
                {
                    additionalAttributes += " target='_blank' rel='nofollow noopener'";
                }
                else if (formatOptions.HasFlag(BasicHtmlFormatOption.LinksNoFollow))
                {
                    additionalAttributes += " rel='nofollow'";
                }
                else if (formatOptions.HasFlag(BasicHtmlFormatOption.LinksToNewWindow))
                {
                    additionalAttributes += " target='_blank' rel='noopener'";
                }

                return $"<a href='{href}'{additionalAttributes}>{displayText}</a>";
            });

            return reg.Replace(text, matchEval);
        }

        public static IHtmlContent ConvertToBasicHtml(string s, BasicHtmlFormatOption formatOptions = BasicHtmlFormatOption.None)
        {
            if (string.IsNullOrEmpty(s)) return new HtmlString(string.Empty);
            var html = ConvertLineBreaksToBrTags(s);
            html = ConvertUrlsToLinks(html, formatOptions);

            return new HtmlString(html);
        }

        public static bool ContainsHtml(string source)
        {
            var tagRegex = new Regex(@"<[^>]+>");
            return tagRegex.IsMatch(source);
        }
    }
}