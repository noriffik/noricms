using System;
using System.Collections;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class TypeHelper
    {
        public static Type GetCollectionTypeOrNull(Type collectionType)
        {
            if (collectionType == null) return null;

            if (!typeof(IEnumerable).IsAssignableFrom(collectionType))
            {
                return null;
            }

            Type singularType = null;
            if (collectionType.IsGenericType)
            {
                var genericParameters = collectionType.GetGenericArguments();

                if (genericParameters?.Length != 1)
                {
                    return null;
                }

                singularType = genericParameters.Single();
            }
            else if (collectionType.IsArray)
            {
                singularType = collectionType.GetElementType();
            }

            return singularType;
        }
    }
}