using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class StringHelper
    {
        public static string FirstNonEmpty(params string[] strings)
        {
            return strings.FirstOrDefault(s => !string.IsNullOrWhiteSpace(s)) ?? string.Empty;
        }

        public static string NullAsEmptyAndTrim(string s)
        {
            return s == null ? string.Empty : s.Trim();
        }

        public static string TrimOrNull(string s)
        {
            if (s == null) return s;

            return s.Trim();
        }

        public static string EmptyAsNull(string s)
        {
            if (s == null || !string.IsNullOrWhiteSpace(s)) return s;
            return null;
        }

        public static bool IsNullOrWhiteSpace(params string[] strings)
        {
            return strings == null || strings.Any(string.IsNullOrWhiteSpace);
        }

        public static string JoinNotEmpty(string separator, params string[] values)
        {
            return string.Join(separator, values.Where(s => !string.IsNullOrWhiteSpace(s)));
        }

        public static IEnumerable<string> SplitAndTrim(string source, params char[] separator)
        {
            if (source == null) return Enumerable.Empty<string>();

            return source
                .Split(separator, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => s.Trim())
                .Where(s => s != string.Empty);
        }

        public static bool IsUpperCase(string s)
        {
            return s != null && s.All(Char.IsUpper);
        }

        public static string RemoveSuffix(string source, string suffix)
        {
            if (source == null) return null;

            return source.EndsWith(suffix) ? source.Remove(source.Length - suffix.Length) : source;
        }

        public static string RemoveSuffix(string source, string suffix, StringComparison stringComparison)
        {
            if (source == null) return null;

            return source.EndsWith(suffix, StringComparison.OrdinalIgnoreCase) ? source.Remove(source.Length - suffix.Length) : source;
        }
    }
}