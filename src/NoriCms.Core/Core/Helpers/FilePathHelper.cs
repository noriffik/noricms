using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class FilePathHelper
    {
        #region constants

        private const char VIRTUAL_PATH_SEPARATOR = '/';
        private const string VIRTUAL_PATH_SEPARATOR_AS_STRING = "/";

        private static char[] ALL_PATH_SEPARATORS = new[] { VIRTUAL_PATH_SEPARATOR, '\\' };

        private static char[] FILE_NAME_INVALID_CHARS = new[] { '\0', '*', '<', '>', '/', '\\', ':', '?', '|', '\"' };
        private static char[] FILE_NAME_TRIM_CHARS = new[] { '$', ' ' };
        private static string[] FILE_NAME_INVALID_VALUES = new[]
        {
            "CON", "PRN", "AUX", "NUL",
            "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
            "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
        };

        #endregion

        public static string CleanFileName(string fileName)
        {
            return CleanFileName(fileName, string.Empty);
        }

        public static string CleanFileName(string fileName, string emptyReplacement)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return emptyReplacement;
            
            var validChars = fileName.Where(x => 
                !Char.IsControl(x) 
                && !FILE_NAME_INVALID_CHARS.Contains(x)
            );

            var newName = String.Concat(validChars).Trim(FILE_NAME_TRIM_CHARS);

            if (string.IsNullOrWhiteSpace(newName) || FILE_NAME_INVALID_VALUES.Contains(newName))
            {
                return emptyReplacement;
            }

            return newName;
        }

        public static bool FileExtensionContainsInvalidChars(string fileExtension)
        {
            var trimmedExtension = fileExtension?.TrimStart('.');

            if (string.IsNullOrWhiteSpace(trimmedExtension)) return false;

            var hasInvalidChars = trimmedExtension.Any(x =>
                x == '.'
                || Char.IsControl(x)
                || FILE_NAME_INVALID_CHARS.Contains(x)
            );

            return hasInvalidChars;
        }

        public static string CombineVirtualPath(params string[] paths)
        {
            var trimmedPaths = paths
                .Where(p => !string.IsNullOrEmpty(p))
                .Select(p => p.Trim(ALL_PATH_SEPARATORS))
                .Where(p => !string.IsNullOrWhiteSpace(p));

            var result = VIRTUAL_PATH_SEPARATOR + string.Join(VIRTUAL_PATH_SEPARATOR_AS_STRING, trimmedPaths);
            return result;
        }
    }
}