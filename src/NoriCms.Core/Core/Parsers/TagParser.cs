using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class TagParser
    {
        public static IEnumerable<string> Split(string tags)
        {
            var tagList = new List<string>();

            if (string.IsNullOrWhiteSpace(tags))
                return tagList;

            // Split by double quotes first
            var doubleQuoteMatches = Regex.Matches(tags, "\"[\\S\\s]*?\"");
            foreach (var match in doubleQuoteMatches.Reverse())
            {
                tagList.Add(match.Value.Trim(new[] { '"' }));
                tags = tags.Remove(match.Index, match.Length);
            }

            // Then split by single quotes
            var singleQuoteMatches = Regex.Matches(tags, "'[\\S\\s]*?'");
            foreach (var match in singleQuoteMatches.Reverse())
            {
                tagList.Add(match.Value.Trim(new[] { '\'' }));
                tags = tags.Remove(match.Index, match.Length);
            }

            // Then split by comma/space
            tagList.AddRange(tags.Split(',', ' ')
                .Where(s => !string.IsNullOrWhiteSpace(s)));

            return tagList
                .Select(s => s.ToLowerInvariant())
                .Distinct()
                .OrderBy(t => t);
        }
    }
}