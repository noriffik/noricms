using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class EnumParser
    {
        private static TEnum? ParseOrNull<TEnum>(string s) where TEnum : struct
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            if (Enum.TryParse<TEnum>(s, true, out var e))
            {
                return e;
            }

            return null;
        }

        public static TEnum ParseOrDefault<TEnum>(string s, TEnum? defaultValue = null) where TEnum : struct
        {
            return ParseOrNull<TEnum>(s) ?? defaultValue ?? default(TEnum);
        }
    }
}