using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class BoolParser
    {
        public static bool? ParseOrNull(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            if (Boolean.TryParse(s, out var b))
            {
                return b;
            }

            return null;
        }

        public static bool ParseOrDefault(string s, bool def = false)
        {
            if (string.IsNullOrWhiteSpace(s)) return def;

            return Boolean.TryParse(s, out var b) ? b : def;
        }
    }
}