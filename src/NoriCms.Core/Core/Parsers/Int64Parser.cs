using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class Int64Parser
    {
        private static long? ParseOrNull(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            long l = 0;
            decimal d = 0;
            if (Int64.TryParse(s, out l))
            {
                return l;
            }
            if (decimal.TryParse(s, out d))
            {
                return Convert.ToInt64(d);
            }

            return null;
        }

        private static long? ParseOrNull(object o)
        {
            if (o == null) return null;
            if (o is long || o is long?)
            {
                return o as long?;
            }
            if (o is decimal || o is decimal?)
            {
                return Convert.ToInt64(o);
            }
            return ParseOrNull(Convert.ToString(o));
        }

        public static long ParseOrDefault(string s, long def = 0)
        {
            return ParseOrNull(s) ?? def;
        }

        public static long ParseOrDefault(object o, int def = 0)
        {
            return ParseOrNull(o) ?? def;
        }
    }
}