using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public class DecimalParser
    {
        private static decimal? ParseOrNull(string s)
        {
            return Decimal.TryParse(s, out var d) ? d : default(decimal?);
        }

        private static decimal? ParseOrNull(object o)
        {
            if (o is decimal || o is decimal?)
            {
                return o as decimal?;
            }
            return ParseOrNull(Convert.ToString(o));
        }

        public static int? ParseToRoundedInt(object o)
        {
            var d = ParseOrNull(o);
            if (!d.HasValue) return null;
            return Convert.ToInt32(Math.Round(d.Value));
        }
    }
}