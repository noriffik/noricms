using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class HttpUriParser
    {
        private static Uri ParseAbsoluteOrDefault(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            if (!s.Contains("://"))
            {
                s = $"http://{s}";
            }

            if (!Uri.TryCreate(s, UriKind.Absolute, out var uri))
            {
                return null;
            }

            return !uri.Scheme.ToLower().StartsWith("http") ? null : uri;
        }

        public static Uri ParseOrDefault(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            var uri = ParseAbsoluteOrDefault(s);

            if (uri != null) return uri;

            return !Uri.TryCreate(s, UriKind.Relative, out uri) ? null : uri;
        }

        public static Uri ParseOrDefault(string s, UriKind kind)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            Uri uri = null;

            if (kind == UriKind.Absolute || kind == UriKind.RelativeOrAbsolute)
            {
                uri = ParseAbsoluteOrDefault(s);
            }

            if (uri != null || kind == UriKind.Absolute) return uri;

            return !Uri.TryCreate(s, kind, out uri) ? null : uri;
        }
    }
}