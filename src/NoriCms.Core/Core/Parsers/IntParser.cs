using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core
{
    public static class IntParser
    {
        private static int? ParseOrNull(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return null;

            if (Int32.TryParse(s, out var i))
            {
                return i;
            }
            if (decimal.TryParse(s, out var d))
            {
                return Convert.ToInt32(d);
            }

            return null;
        }

        private static int? ParseOrNull(object o)
        {
            if (o == null) return null;
            if (o is int || o is int?)
            {
                return o as int?;
            }
            if (o is decimal || o is decimal?)
            {
                return Convert.ToInt32(o);
            }
            return ParseOrNull(Convert.ToString(o));
        }

        public static int ParseOrDefault(string s, int def = 0)
        {
            return ParseOrNull(s) ?? def;
        }

        public static int ParseOrDefault(object o, int def = 0)
        {
            return ParseOrNull(o) ?? def;
        }

        public static IEnumerable<int> ParseFromDelimitedString(string list)
        {
            return ParseFromDelimitedString(list, new[] { ',' });
        }

        private static IEnumerable<int> ParseFromDelimitedString(string str, params char[] delimiter)
        {
            if (string.IsNullOrEmpty(str)) return Enumerable.Empty<int>();

            return StringHelper
                .SplitAndTrim(str, delimiter)
                .Select(ParseOrNull)
                .FilterNotNull();
        }
    }
}