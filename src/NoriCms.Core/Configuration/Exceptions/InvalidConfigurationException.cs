using System;
using System.Collections.Generic;
using System.Linq;
using NoriCms.Core.Validation;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Configuration
{
    public class InvalidConfigurationException : Exception
    {
        public InvalidConfigurationException(Type configType, IEnumerable<ValidationError> errors)
            : base(GetMessage(configType?.Name, errors?.Select(e => e.Message).FirstOrDefault()))
        {
        }
        
        public InvalidConfigurationException(Type configType, string errorMessage)
            : base(GetMessage(configType?.Name, errorMessage))
        {
        }
        
        public InvalidConfigurationException(string configName, string errorMessage)
            : base(GetMessage(configName, errorMessage))
        {
        }

        private static string GetMessage(string configName, string errorMessage)
        {
            return $"{configName} configuration invalid: {errorMessage}";
        }
    }
}