using NoriCms.Core.Di.AppDi;
using NoriCms.Core.Di.ContainerSpecific;

namespace NoriCms.Core.Configuration.Bootstrap
{
    public class ConfigurationDependencyRegistration: IDependencyRegistration
    {
        public void Register(IContainerRegister container)
        {
            var singleInstanceRegistrationOptions = RegistrationOptions.SingletonScope();

            container
                .RegisterGeneric(typeof(IConfigurationSettingsFactory<>), typeof(ConfigurationSettingsFactory<>), singleInstanceRegistrationOptions)
                .RegisterAllWithFactory(typeof(IConfigurationSettings), typeof(IConfigurationSettingsFactory<>), singleInstanceRegistrationOptions);
        }
    }
}