// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Configuration
{
    [NamespacedConfigurationSetting("NoriCms:Plugins")]
    public abstract class PluginConfigurationSettingsBase : IConfigurationSettings
    {
    }
}