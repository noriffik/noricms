using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NoriCms.Core.Validation;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Configuration
{
    public class ConfigurationSettingsFactory<TSettings> : IConfigurationSettingsFactory<TSettings> where TSettings : class, IConfigurationSettings, new()
    {
        #region constructor

        private readonly IServiceProvider _serviceProvider;
        private readonly IModelValidationService _modelValidationService;

        public ConfigurationSettingsFactory(IServiceProvider serviceProvider,
            IModelValidationService modelValidationService)
        {
            _serviceProvider = serviceProvider;
            _modelValidationService = modelValidationService;
        }

        #endregion

        #region public

        /// <summary>
        /// Creates an instance of a settings objects, extracting setting values from
        /// a configuration source.
        /// </summary>
        public TSettings Create()
        {
            var settingsOptions = _serviceProvider.GetRequiredService<IOptions<TSettings>>();
            var settings = settingsOptions.Value;

            if (settings is IFeatureEnableable featureEnableable && !featureEnableable.Enabled)
            {
                // feature is disabled, so skip validation.
                return settings;
            }

            var errors = _modelValidationService.GetErrors(settings);

            var validationErrors = errors.ToList();
            if (!EnumerableHelper.IsNullOrEmpty(validationErrors))
            {
                throw new InvalidConfigurationException(typeof(TSettings), validationErrors);
            }

            return settings;
        }

        #endregion
        
    }
}