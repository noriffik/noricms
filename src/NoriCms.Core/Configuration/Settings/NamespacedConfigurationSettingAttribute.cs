using System;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Configuration
{
    [AttributeUsage(AttributeTargets.Class)]
    public class NamespacedConfigurationSettingAttribute : Attribute
    {
        public NamespacedConfigurationSettingAttribute(string settingNamespace)
        {
            Namespace = settingNamespace;
        }
        public string Namespace { get; private set; }
    }
}