// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Configuration
{
    public interface IFeatureEnableable
    {
        public bool Enabled { get; }
    }
}