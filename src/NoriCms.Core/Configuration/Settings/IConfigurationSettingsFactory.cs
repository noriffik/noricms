using NoriCms.Core.Di.AppDi;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Configuration
{
    public interface IConfigurationSettingsFactory<out TSettings> : IInjectionFactory<TSettings> where TSettings : class, IConfigurationSettings, new()
    {
    }
}