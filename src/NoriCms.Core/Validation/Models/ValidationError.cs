using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class ValidationError
    {
        public ValidationError()
        {
        }
        
        public ValidationError(string message, string property = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentNullException( nameof(message));
            }

            Message = message;
            if (string.IsNullOrWhiteSpace(property))
            {
                Properties = new string[1] { property };
            }
        }
        
        public ICollection<string> Properties { get; set; }

        public string Message { get; set; }

        public string ErrorCode { get; set; }
    }
}