using NoriCms.Core.Di.AppDi;
using NoriCms.Core.Di.ContainerSpecific;

namespace NoriCms.Core.Validation.Bootstrap
{
    public class ValidationDependencyRegistration : IDependencyRegistration
    {
        public void Register(IContainerRegister container)
        {
            container.RegisterSingleton<IModelValidationService, ModelValidationService>();
        }
    }
}