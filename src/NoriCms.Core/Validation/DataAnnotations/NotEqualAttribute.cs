using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class NotEqualAttribute : ValidationAttribute
    {
        public string OtherProperty { get; private set; }
        public NotEqualAttribute(string otherProperty)
        {
            OtherProperty = otherProperty;
            ErrorMessage = $"The {{0}} field cannot be the same value as the {otherProperty} field";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetTypeInfo().GetProperty(OtherProperty);
            if (property == null)
            {
                throw new ArgumentException(
                    $"{OtherProperty} is not a property of {validationContext.ObjectType.FullName}");
            }

            var otherValue = property.GetValue(validationContext.ObjectInstance, null);
            return Equals(value, otherValue) 
                ? new ValidationResult(FormatErrorMessage(validationContext.DisplayName), new[] { validationContext.MemberName }) 
                : null;
        }
    }
}