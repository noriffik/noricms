using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class TagTextAttribute : RegularExpressionAttribute
    {
        public TagTextAttribute()
            : base(@"^[&\w\s'()-]+$")
        {
            ErrorMessage = "The {0} field contains invalid characters.";
        }
    }
}