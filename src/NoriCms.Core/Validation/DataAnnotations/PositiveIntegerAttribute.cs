using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class PositiveIntegerAttribute : RangeAttribute
    {
        public PositiveIntegerAttribute() :
            base(1, Int32.MaxValue)
        {
            ErrorMessage = "The {0} field must be a positive integer value.";
        }
    }
}