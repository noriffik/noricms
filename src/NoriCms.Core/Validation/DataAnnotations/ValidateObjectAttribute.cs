using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class ValidateObjectAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            // if this a collection, validate each item.
            if (value is IEnumerable enumerable)
            {
                foreach (var collectionValue in enumerable)
                {
                    ValidateValue(collectionValue, results);
                }
            }
            else
            {
                ValidateValue(value, results);
            }

            if (results.Count == 0) return ValidationResult.Success;
            var msg = $"{validationContext.DisplayName} validation failed";
            var compositeResults = new CompositeValidationResult(msg, new[] { validationContext.MemberName });
            results.ForEach(compositeResults.AddResult);

            return compositeResults;
        }

        private void ValidateValue(object value, List<ValidationResult> results)
        {
            // ValidationContext constructor requires value to not be null.
            if (value == null) return;

            var context = new ValidationContext(value, null, null);

            Validator.TryValidateObject(value, context, results, true);
        }
    }
}