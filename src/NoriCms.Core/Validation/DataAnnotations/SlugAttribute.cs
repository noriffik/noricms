using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class SlugAttribute : RegularExpressionAttribute
    {
        public SlugAttribute() :
            base("^[a-zA-Z0-9-]+$")
        {
            ErrorMessage = "{0} can only use letters, numbers and hyphens.";
        }
    }
}