using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class ValidationErrorException : ValidationException
    {
        public ValidationErrorException() : base()
        {
        }
        
        public ValidationErrorException(string message) : base(message)
        {
        }
        
        public ValidationErrorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
        
        public ValidationErrorException(ValidationError validationError)
            : base(new ValidationResult(validationError?.Message, validationError?.Properties), null, null)
        {
            ErrorCode = validationError.ErrorCode;
        }
        
        public string ErrorCode { get; private set; }
        
        public static ValidationErrorException CreateWithProperties(string message, params string[] properties)
        {
            return new ValidationErrorException(new ValidationError
            {
                Message = message,
                Properties = properties
            });
        }

        public static ValidationErrorException CreateWithCode(string message, string code)
        {
            return new ValidationErrorException(new ValidationError
            {
                Message = message,
                ErrorCode = code
            });
        }

        public static ValidationErrorException CreateWithCodeAndProperties(string message, string code,
            params string[] properties)
        {
            return new ValidationErrorException(new ValidationError
            {
                Message = message,
                ErrorCode = code,
                Properties = properties
            });
        }
    }
}