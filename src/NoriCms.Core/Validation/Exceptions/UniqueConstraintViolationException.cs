using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class UniqueConstraintViolationException : ValidationException
    {
        public UniqueConstraintViolationException()
        {
        }

        public UniqueConstraintViolationException(string message)
            : base(message)
        {
        }

        protected UniqueConstraintViolationException(string message, string property, object value = null)
            : base(GetValidationResult(message, new string[] { property }), null, value)
        {
        }

        public static void ThrowIfExists<TEntity>(TEntity e, string property) where TEntity : class
        {
            if (e != null)
            {
                throw new UniqueConstraintViolationException<TEntity>(property);
            }
        }

        private static ValidationResult GetValidationResult(string message, IEnumerable<string> properties)
        {
            var vr = new ValidationResult(message, properties);
            return vr;
        }
    }
    
    public class UniqueConstraintViolationException<TEntity> : UniqueConstraintViolationException where TEntity : class
    {
        private const string errorMessage = "Entity of type '{0}' failed a uniqueness check on property {1}";
        private const string errorMessageWithoutValue = errorMessage + ".";
        private const string errorMessageWithValue = errorMessage + " with value {2}.";

        public UniqueConstraintViolationException(string property)
            : base(FormatMessage(property, null), property)
        {
        }

        public UniqueConstraintViolationException(string property, object value = null)
            : base(FormatMessage(property, value), property, value)
        {
        }

        private static string FormatMessage(string property, object value)
        {
            return value == null ? string.Format(errorMessageWithoutValue, typeof(TEntity).Name, property) : string.Format(errorMessageWithValue, typeof(TEntity).Name, property, value.ToString());
        }
    }
}