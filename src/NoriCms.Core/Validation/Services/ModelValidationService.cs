using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public class ModelValidationService : IModelValidationService
    {
        public virtual void Validate<T>(T modelToValidate)
        {
            if (modelToValidate == null)
            {
                throw new ArgumentNullException(nameof(modelToValidate));
            }

            var cx = new ValidationContext(modelToValidate);
            Validator.ValidateObject(modelToValidate, cx, true);
        }

        public virtual IEnumerable<ValidationError> GetErrors<T>(IEnumerable<T> modelsToValidate)
        {
            foreach (var model in modelsToValidate)
            {
                var errors = GetErrors(model);
                foreach (var error in errors)
                {
                    yield return error;
                }
            }
        }

        public virtual IEnumerable<ValidationError> GetErrors<T>(T modelToValidate)
        {
            if (modelToValidate == null)
            {
                throw new ArgumentNullException("modelToValidate");
            }

            var validationResults = new List<ValidationResult>();
            var cx = new ValidationContext(modelToValidate);
            Validator.TryValidateObject(modelToValidate, cx, validationResults, true);

            foreach (var result in validationResults)
            {
                if (result is CompositeValidationResult)
                {
                    var compositeResult = (CompositeValidationResult)result;
                    foreach (var childResult in compositeResult.Results)
                    {
                        yield return MapErrors(childResult);
                    }
                }
                else
                {
                    yield return MapErrors(result);
                }
            }
        }

        protected ValidationError MapErrors(ValidationResult result)
        {
            var error = new ValidationError
            {
                Message = result.ErrorMessage, 
                Properties = result.MemberNames.ToArray()
            };
            return error;
        }
    }
}