using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace NoriCms.Core.Validation
{
    public interface IModelValidationService
    {
        void Validate<T>(T commandToValidate);
        
        IEnumerable<ValidationError> GetErrors<T>(T modelToValidate);
        
        IEnumerable<ValidationError> GetErrors<T>(IEnumerable<T> modelsToValidate);
    }
}